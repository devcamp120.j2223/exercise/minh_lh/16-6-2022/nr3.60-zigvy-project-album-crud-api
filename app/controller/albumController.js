// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const albumModel = require('../model/albumModel');
// Post create new user
const createAlbum = (request, response) => {
    let body = request.body;
    if (!body.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "title is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let albumCreate = {
        _id: mongoose.Types.ObjectId(),
       title:body.title,
    }
    albumModel.create(albumCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create album successfully",
                data: data
            })
        }
    })
}
// get all 
const getAllAlbum = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    albumModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all album success",
                data: data
            })
        }
    })
}
// put all 
const putAlbum = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "album ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateAlbum = {
       title:body.title,
    }
    albumModel.findByIdAndUpdate(albumId, updateAlbum, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update update success",
                data: data
            })
        }
    })
}
// getUserById all 
const AlbumGetById = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " album ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    albumModel.findById(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get album by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deleteAlbumById = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " album ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    albumModel.findOneAndDelete(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete album success"
            })
        }
    })
}
// post of userid
//GET POSTS OF USER
const getAlbumOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    albumModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All albums Of users Id: " + userId,
                data: data
            })
        }
    })
}
//getAllPost
const getAllAlbumQuery = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    let userId = request.query.userId

    albumModel.find(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all album success",
                data: data
            })
        }
    })
}
//export 
module.exports = { createAlbum, getAllAlbum, putAlbum, AlbumGetById, deleteAlbumById, getAlbumOfUser, getAllAlbumQuery }