// import mongoose
const mongoose = require('mongoose');
// tao scheme
const Schema = mongoose.Schema
// tạo schema model
const albumSchema = new Schema({
    userId: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user"
        }
    ],
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        required: true,
    }

})

//export modle
module.exports = mongoose.model("album", albumSchema)