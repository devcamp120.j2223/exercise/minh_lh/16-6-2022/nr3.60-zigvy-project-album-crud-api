// import express
const express = require('express');
// import mongoose
const mongoose = require('mongoose');
// tạo cổng
const port = 8000;
// khởi tạo app
const app = express();
// khởi tạo json
app.use(express.json());
// chạy trên unicode
app.use(express.urlencoded({
    extended: true
}));
// import router
const albumRouter = require('../nr3.60-zigvy-project-album-crud-api/app/router/albumRouter')
const userRouter = require('../nr3.60-zigvy-project-album-crud-api/app/router/userRouter')
// chạy mongoose
mongoose.connect("mongodb://localhost:27017/123", (error) => {
    if (error) {
        throw error
    }
    console.log("mongodb successfully established")
});
//sử dụng router
app.use("/", albumRouter);
app.use("/", userRouter);
// console cổng
app.listen(port, () => {
    console.log('listening on port ' + port);
});